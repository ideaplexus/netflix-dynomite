FROM redis:alpine AS redis-intermediate
FROM alpine:3.12 AS dynomite-intermediate

ARG DYNOMITE_VERSION
ARG SSL=openssl-dev

RUN set -eux \
  && apk upgrade --no-cache \
  && apk add --no-cache \
    git \
    build-base \
    autoconf \
    automake \
    libtool \
    ${SSL} \
  && git clone https://github.com/Netflix/dynomite.git /dynomite \
  && cd /dynomite \
  && git checkout v${DYNOMITE_VERSION} \
  && autoreconf -fvi \
  && ./configure \
  && make \
  && make install

FROM alpine

COPY --from=redis-intermediate /usr/local/bin/redis-server /usr/local/bin/
COPY --from=dynomite-intermediate /usr/local/sbin/dynomite /usr/local/bin/
COPY --from=dynomite-intermediate /dynomite/conf/recon_iv.pem /conf/recon_iv.pem
COPY --from=dynomite-intermediate /dynomite/conf/recon_key.pem /conf/recon_key.pem
COPY dynomite.yml /conf/dynomite.yml
COPY redis.conf /conf/redis.conf
COPY supervisord.conf /etc/supervisord.conf

RUN set -eux \
  && RUN_DEPS="$( \
    scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
      | tr ',' '\n' \
      | sort -u \
      | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )" \
  && apk upgrade --no-cache \
  && apk add --no-cache \
    bash \
    coreutils \
    supervisor \
    $RUN_DEPS \
  && mkdir -p \
    /data/redis

EXPOSE 8101
EXPOSE 8102
EXPOSE 22122
EXPOSE 22222

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]
